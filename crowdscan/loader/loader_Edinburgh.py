# Author: Pat
# Email:bingqing.zhang.18@ucl.ac.uk

import numpy as np
import pandas as pd
import glob
import ast
import cv2
from crowdscan.crowd.trajdataset import TrajDataset 

def get_homog():
    image_9points = [[155,86.6],[350,95.4],[539,106],[149,206],[345,215],[537,223],[144,327],[341,334],[533,340]]
    #use top left points as origin
    image_9points_shifted = [[x-image_9points[0][0],y-image_9points[0][1]] for x,y in image_9points] 
    #world plane points
    d_vert = 2.97
    d_hori = 4.85
    world_9points=[]
    for i in range(3):
        for j in range(3):
            world_9points.append([d_hori*j,d_vert*i])
        
    #find homog matrix   
    h, status = cv2.findHomography(np.array(image_9points_shifted), np.array(world_9points))

    return h

# loaded all Edinburgh tracks files together
def loadEdinburgh(path, **kwargs):
    traj_dataset = TrajDataset()
    traj_dataset.title = "Edinburgh"
    files_list = sorted(glob.glob(path + "/*.txt"))
    csv_columns = ['centre_x','centre_y','frame','agent_id'] 
    # read from csv => fill traj table 
    raw_dataset = []
    scene = []
    last_scene_frame = 0
    new_id = 0
    scale = 0.0247
    # load data from all files
    for file in files_list:
        data = pd.read_csv(file, sep="\n|=", header=None,index_col=None)
        data.reset_index(inplace =True)
        data = data[data['index'].str.startswith('TRACK')]
        #reconstruct the data in arrays 
        track_data = []
        print("reading:"+str(file))
        for row in range(len(data)):
            one_track = data.iloc[row,1].split(";")
            one_track.pop()
            one_track[0] = one_track[0].replace('[[','[')
            one_track[-1] = one_track[-1].replace(']]',']')
            one_track =np.array([ast.literal_eval(i.replace(' [','[').replace(' ',',')) for i in one_track])
            one_track = np.c_[one_track, np.ones(one_track.shape[0],dtype=int)*row] 
            track_data.extend(one_track)
        scene.extend([files_list.index(file)]*len(track_data))
        
        #re-id
        track_data = np.array(track_data)
        uid=np.unique(track_data[:,1])
        for oneid in uid:
            oneid_idx = [idx for idx, x in enumerate(track_data[:,1]) if x == oneid]
            for j in oneid_idx:
                track_data[j,1] = new_id
            new_id +=1
            
        raw_dataset.extend(track_data.tolist())
    raw_dataset = pd.DataFrame(np.array(raw_dataset), columns=csv_columns)
    raw_dataset.reset_index(inplace=True, drop=True)
    
    #find homog matrix
    H = get_homog()
    #apply H matrix to the image point
    img_data = raw_dataset[["centre_x","centre_y"]].values
    world_data = []
    for row in img_data:
        augImg_data=np.c_[[row],np.array([1])]
        world_data.append(np.matmul(H,augImg_data.reshape(3,1)).tolist()[:2])
        
    raw_dataset["centre_x"] = np.array(world_data)[:,0]
    raw_dataset["centre_y"] = np.array(world_data)[:,1]
    
    
    traj_dataset.fps = kwargs.get('fps', 9)
    traj_dataset.data[["frame_id", "agent_id","pos_x", "pos_y"]] = raw_dataset[["frame", "agent_id","centre_x","centre_y"]]
    traj_dataset.data["scene_id"] = scene

    N = len(traj_dataset.data)
    traj_dataset.groupmates = dict(zip(np.unique(traj_dataset.data["agent_id"]), [[] for _ in range(N)]))
    # post-process. For Edinburgh, raw data do not include velocity, velocity info is postprocessed
    traj_dataset.postprocess()
    return traj_dataset
    

