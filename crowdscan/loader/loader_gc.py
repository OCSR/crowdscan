# Author: Javad Amirian
# Email: amiryan.j@gmail.com

import json
import pandas as pd
import numpy as np
import os
from crowdscan.crowd.trajdataset import TrajDataset


def image_to_world(p, Homog):
    pp = np.stack((p[:, 0], p[:, 1], np.ones(len(p))), axis=1)
    PP = np.matmul(Homog, pp.T).T
    P_normal = PP / np.repeat(PP[:, 2].reshape((-1, 1)), 3, axis=1)
    return P_normal[:, :2]


def loadGC(path, **kwargs):
    traj_dataset = TrajDataset()

    dir_list = sorted(os.listdir(path))
    raw_data = []

    selected_frames = kwargs.get("frames", range(0, 120001))

    for dir_name in dir_list:
        person_trajectory_txt_path = os.path.join(path, dir_name)
        agent_id = int(dir_name.replace('.txt', ''))

        with open(person_trajectory_txt_path, 'r') as f:
            trajectory_list = f.read().split()
            for i in range(len(trajectory_list) // 3):
                py = float(trajectory_list[3 * i])
                px = float(trajectory_list[3 * i + 1])
                frame_id = int(trajectory_list[3 * i + 2])
                if selected_frames.start <= frame_id < selected_frames.stop:
                    raw_data.append([frame_id, agent_id, px, py])

    csv_columns = ["frame_id", "agent_id", "pos_x", "pos_y"]
    raw_dataset = pd.DataFrame(np.array(raw_data), columns=csv_columns)

    homog = []
    homog_file = kwargs.get("homog_file", "")
    if os.path.exists(homog_file):
        with open(homog_file) as f:
            homog_str = f.read()
            homog = np.array(json.loads(homog_str)['homog'])

    # homog = [[4.97412897e-02, -4.24730883e-02, 7.25543911e+01],
    #          [1.45017874e-01, -3.35678711e-03, 7.97920970e+00],
    #          [1.36068797e-03, -4.98339188e-05, 1.00000000e+00]]

    if len(homog):
        world_coords = image_to_world(raw_dataset[["pos_x", "pos_y"]].to_numpy(), homog)
        raw_dataset[["pos_x", "pos_y"]] = pd.DataFrame(world_coords)

    # copy columns
    traj_dataset.data[["frame_id", "agent_id", "pos_x", "pos_y"]] = \
        raw_dataset[["frame_id", "agent_id", "pos_x", "pos_y"]]

    traj_dataset.title = kwargs.get('title', "Grand Central")
    traj_dataset.fps = kwargs.get('fps', 25)
    traj_dataset.data["scene_id"] = kwargs.get('scene_id', 0)
    traj_dataset.data["label"] = "pedestrian"

    # post-process
    use_kalman_smoother = kwargs.get('use_kalman_smoother', False)
    traj_dataset.postprocess(use_kalman_smoother=use_kalman_smoother)

    interpolate = kwargs.get('interpolate', False)
    if interpolate:
        traj_dataset.interpolate_frames()

    return traj_dataset
