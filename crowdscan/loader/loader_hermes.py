# Author: Javad Amirian
# Email: amiryan.j@gmail.com

import pandas as pd
import numpy as np
import sys
import os
from crowdscan.crowd.trajdataset import TrajDataset


def loadHermes(path, **kwargs):
    path = '/home/cyrus/workspace2/OpenTraj/datasets/HERMES/Corridor-1D/uo-050-180-180.txt'
    traj_dataset = TrajDataset()

    # id = int(row[0])
    # ts = int(row[1])
    # px = -float(row[3]) / 100.
    # py = float(row[2]) / 100.

    csv_columns = ["agent_id", "frame_id", "pos_x", "pos_y", "pos_z"]
    # read from csv => fill traj table
    raw_dataset = pd.read_csv(path, sep=r"\s+", header=None, names=csv_columns)

    # convert from cm => meter
    raw_dataset["pox_x"] = raw_dataset["pox_x"] / 100.
    raw_dataset["pox_y"] = raw_dataset["pox_y"] / 100.

    traj_dataset.title = kwargs.get('title', "no_title")
    traj_dataset.fps = kwargs.get('fps', 16)

    # copy columns
    traj_dataset.data[["frame_id", "agent_id", "pos_x", "pos_y"]] = \
        raw_dataset[["frame_id", "agent_id", "pos_x", "pos_y"]]

    traj_dataset.data["scene_id"] = kwargs.get('scene_id', 0)
    traj_dataset.data["label"] = "pedestrian"

    N = len(traj_dataset.data)
    traj_dataset.groupmates = dict(zip(np.unique(traj_dataset.data["agent_id"]), [[] for _ in range(N)]))  # TODO

    # post-process
    traj_dataset.postprocess()

    return traj_dataset