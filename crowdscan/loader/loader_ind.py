# Jean-Bernard Hayet
# jbhayet@cimat.mx

import pathlib

import numpy as np
import pandas as pd

from crowdscan.crowd.trajdataset import TrajDataset


def load_ind(path, **kwargs):
    ind_dataset = TrajDataset()
    # Note: we assume here that the path that is passed is the one to the tracks CSV.

    # Read the tracks
    raw_dataset = pd.read_csv(path, sep=",", header=0,
                              names=["recordingId", "trackId", "frame", "trackLifetime", "xCenter", "yCenter",
                                     "heading", "width", "length", "xVelocity", "yVelocity", "xAcceleration",
                                     "yAcceleration", "lonVelocity", "latVelocity", "lonAcceleration",
                                     "latAcceleration"])

    # Read the recording data
    data_path = pathlib.Path(path)
    datadir_path = data_path.parent
    recording_path = str(datadir_path) + '/{:02d}_recordingMeta.csv'.format(raw_dataset['recordingId'][0])
    recording_data = pd.read_csv(recording_path, sep=",", header=0,
                                 names=["recordingId", "locationId", "frameRate", "speedLimit", "weekday", "startTime",
                                        "duration", "numTracks", "numVehicles", "numVRUs", "latLocation", "lonLocation",
                                        "xUtmOrigin", "yUtmOrigin", "orthoPxToMeter"])
    ind_dataset.fps = int(recording_data["frameRate"][0])
    ind_dataset.title = kwargs.get('title', "no_title")

    # Read the meta-tracks data
    tracks_path = str(datadir_path) + '/{:02d}_tracksMeta.csv'.format(raw_dataset['recordingId'][0])
    tracks_data = pd.read_csv(tracks_path, sep=",", header=0,
                              names=["recordingId", "trackId", "initialFrame", "finalFrame", "numFrames", "width",
                                     "length", "class"])
    # Get the ids of pedestrians only
    ped_ids = tracks_data[tracks_data["class"] == "pedestrian"]["trackId"].values
    raw_dataset = raw_dataset[raw_dataset['trackId'].isin(ped_ids)]

    # Copy columns
    ind_dataset.data[["frame_id", "agent_id",
                      "pos_x", "pos_y", "vel_x", "vel_y"]] = \
        raw_dataset[["frame", "trackId",
                     "xCenter", "yCenter", "xVelocity", "yVelocity"]]

    ind_dataset.data["label"] = "pedestrian"

    # post-process
    ind_dataset.postprocess()
    return ind_dataset
