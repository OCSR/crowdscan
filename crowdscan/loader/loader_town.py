# Francisco Valente Castro
# francisco.valente@cimat.mx

import cv2
import pathlib
import numpy as np
import pandas as pd

from crowdscan.crowd.trajdataset import TrajDataset


def read_projection_parameters(path):
    # Read calibration file
    calibration_file = open(path, 'r')

    # Save calibration parameters in dictionary
    param = {}
    for line in calibration_file:
        name, _, value = line.split()
        param[name] = float(value)

    # Rotation vector
    rvec = np.array([param['RotationX'],
                     param['RotationY'],
                     param['RotationZ'],
                     param['RotationW']])

    # Translation vector
    tvec = np.array([param['TranslationX'],
                     param['TranslationY'],
                     param['TranslationZ']])

    # Camera matrix
    cameraMatrix = np.array([[param['FocalLengthX'],
                              0.0,
                              param['PrincipalPointX']],
                             [0.0, param['FocalLengthY'],
                              param['PrincipalPointY']],
                             [0.0, 0.0, 1.0]])

    # Distortion coefficients
    distCoeffs = np.array([param['DistortionK1'],
                           param['DistortionK2'],
                           param['DistortionP1'],
                           param['DistortionP2']])

    return rvec, tvec, cameraMatrix, distCoeffs


def obtainObjectPoints(pts, rvec, tvec, cameraMatrix, distCoeffs):
    # Obtain variables values
    dim_0 = pts.shape[0]

    # Obtain homogeneus points
    homoPts = np.concatenate([pts, np.ones([dim_0, 1])], axis=1)

    # FIXME : Undistort points
    # undPts = cv2.undistortPoints(homoPts, cameraMatrix, distCoeffs)

    # Construct rotation matrix from quaternion
    qx, qy, qz, qw = rvec
    rotation = [[1 - 2 * qy ** 2 - 2 * qz ** 2,
                 2 * qx * qy - 2 * qz * qw,
                 2 * qx * qz + 2 * qy * qw],
                [2 * qx * qy + 2 * qz * qw,
                 1 - 2 * qx ** 2 - 2 * qz ** 2,
                 2 * qy * qz - 2 * qx * qw],
                [2 * qx * qz - 2 * qy * qw,
                 2 * qy * qz + 2 * qx * qw,
                 1 - 2 * qx ** 2 - 2 * qy ** 2]]

    # Get projection matrix
    proj = cameraMatrix @ rotation
    inv_proj = np.linalg.inv(proj)

    # Get un-projected points
    unprojPts = (inv_proj @ homoPts.T).T - tvec

    return unprojPts


def loadTownCenter(path, **kwargs):
    # Construct dataset
    town_dataset = TrajDataset()

    # Note: we assume here that the path that is passed is the one to the tracks CSV.
    # Read the tracks
    raw_dataset = pd.read_csv(path, sep=",", header=0,
                              names=["personNumber", "frameNumber", "headValid", "bodyValid", "headLeft", "headTop",
                                     "headRight", "headBottom", "bodyLeft", "bodyTop", "bodyRight", "bodyBottom"])

    # Get center of bounding boxes
    raw_dataset["body_x"] = (raw_dataset["bodyLeft"] + raw_dataset["bodyRight"]) / 2.0
    raw_dataset["body_y"] = (raw_dataset["bodyTop"] + raw_dataset["bodyBottom"]) / 2.0

    # FIXME : Take the lower point, Points on the ground

    raw_dataset["head_x"] = (raw_dataset["headLeft"] + raw_dataset["headRight"]) / 2.0
    raw_dataset["head_y"] = (raw_dataset["headTop"] + raw_dataset["headBottom"]) / 2.0

    # Required information
    raw_dataset["label"] = "pedestrian"

    # Read camera calibration
    calibration_path = kwargs.get('calib_path', 'none')
    rvec, tvec, cameraMatrix, distCoeffs =\
        read_projection_parameters(calibration_path)

    # Obtain real world coordinates from image
    pts = np.array([raw_dataset["body_x"], raw_dataset["body_y"]]).T
    objPts = obtainObjectPoints(pts, rvec, tvec,
                                cameraMatrix, distCoeffs)

    # Add object points to raw dataset
    raw_dataset['pos_x'] = objPts[:, 0]
    raw_dataset['pos_y'] = objPts[:, 1]
    raw_dataset['pos_z'] = objPts[:, 2]

    # FIXME : Remove invalid body bouding boxes

    # Recording information
    town_dataset.fps = kwargs.get('fps', 25)
    town_dataset.title = kwargs.get('title', "Oxford Town Center")

    # Copy columns
    town_dataset.data[["frame_id", "agent_id", "pos_x", "pos_y", "pos_z"]] = \
        raw_dataset[["frameNumber", "personNumber", "pos_x", "pos_y", "pos_z"]]

    # post-process
    town_dataset.postprocess()
    return town_dataset
