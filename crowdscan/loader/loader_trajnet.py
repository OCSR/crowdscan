# Author: Javad Amirian
# Email: amiryan.j@gmail.com

import numpy as np
import pandas as pd
from crowdscan.crowd.trajdataset import TrajDataset


def loadTrajNet(path, **kwargs):
    trajnet_dataset = TrajDataset()
    trajnet_dataset.title = kwargs.get('title', "TrajNet")

    csv_columns = ["frame_id", "agent_id", "pos_x", "pos_y"]

    # read from csv => fill traj
    raw_dataset = pd.read_csv(path, sep=" ", header=None, names=csv_columns)
    raw_dataset.replace('?', np.nan, inplace=True)
    raw_dataset.dropna(inplace=True)
    if 'stanford' in path:
        trajnet_dataset.fps = 30
    elif 'crowd' in path or 'biwi' in path:
        trajnet_dataset.fps = 16
    else:
        trajnet_dataset.fps = 7

    # FIXME: in the cases you load more than one file into a TrajDataset Object

    # rearrange columns
    trajnet_dataset.data[["frame_id", "agent_id", "pos_x", "pos_y"]] = \
        raw_dataset[["frame_id", "agent_id", "pos_x", "pos_y"]]
    
    trajnet_dataset.data["scene_id"] = kwargs.get("scene_id", 0)

    # calculate velocities + perform some checks
    trajnet_dataset.postprocess()

    return trajnet_dataset
