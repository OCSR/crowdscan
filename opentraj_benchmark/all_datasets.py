# Author: Javad Amirian
# Email: amiryan.j@gmail.com

import os
import glob
import yaml
import numpy as np
import pandas as pd

from crowdscan.crowd.trajdataset import TrajDataset, merge_datasets
from crowdscan.loader.loader_eth import loadETH
from crowdscan.loader.loader_gc import loadGC
from crowdscan.loader.loader_trajnet import loadTrajNet
from crowdscan.loader.loader_sdd import loadSDD_single, loadSDD_all
from opentraj_benchmark.constvel import const_vel
from opentraj_benchmark.trajlet import split_trajectories


all_dataset_names = [
    'eth-univ',
    'eth-hotel',
    'ucy-zara-1',
    'ucy-zara-2',
    'ucy-univ-3',
    'gc',
    'sdd-bookstore-0',
    # ...
    'trajnet-mot'
    # ...
]


# ================================================================
# ===================== Load the datasets ========================
# ================================================================
def get_datasets(opentraj_root, dataset_names):
    datasets = []    
    # ========== ETH ==============
    if 'eth-univ' in dataset_names:
        eth_univ_root = os.path.join(opentraj_root, 'datasets/ETH/seq_eth/obsmat.txt')
        eth_univ_dataset = loadETH(eth_univ_root, title='Univ', scene_id='Univ')
        datasets.append(eth_univ_dataset)

    if 'eth-hotel' in dataset_names:
        eth_hotel_root = os.path.join(opentraj_root, 'datasets/ETH/seq_hotel/obsmat.txt')
        eth_hotel_dataset = loadETH(eth_hotel_root, title='Hotel', scene_id='Hotel')
        datasets.append(eth_hotel_dataset)
    # ******************************

    # ========== UCY ==============
    if 'ucy-zara-1' in dataset_names:
        zara01_root = os.path.join(opentraj_root, 'datasets/UCY/zara01/obsmat.txt')
        zara01_dataset = loadETH(zara01_root, title='zara01')
        datasets.append(zara01_dataset)

    if 'ucy-zara-2' in dataset_names:
        zara02_root = os.path.join(opentraj_root, 'datasets/UCY/zara02/obsmat.txt')
        zara02_dataset = loadETH(zara02_root, title='zara02')
        datasets.append(zara02_dataset)

    if 'ucy-univ-3' in dataset_names:
        students03_root = os.path.join(opentraj_root, 'datasets/UCY/students03/obsmat.txt')
        students03_dataset = loadETH(students03_root, title='students03')
        datasets.append(students03_dataset)
    # ******************************

    # ========== GC ==============
    if 'gc' in dataset_names:
        gc_root = os.path.join(opentraj_root, 'datasets/GC/Annotation')
        gc_dataset = loadGC(gc_root, world_coord=True)
        datasets.append(gc_dataset)
    # ******************************

    # ========== SDD ==============
    sdd_root = os.path.join(opentraj_root, 'datasets', 'SDD') 
    search_filter_str = "**/annotations.txt"
    if not sdd_root.endswith("/"):
        search_filter_str = "/" + search_filter_str
    files_list = sorted(glob.glob(sdd_root + search_filter_str, recursive=True))
    scales_yaml_file = os.path.join(sdd_root, 'estimated_scales.yaml')
    with open(scales_yaml_file, 'r') as f:
        scales_yaml_content = yaml.load(f, Loader=yaml.FullLoader)

    for file in files_list:
        dir_names = file.split('/')
        scene_name = dir_names[-3]
        scene_video_id = dir_names[-2]
        if ('sdd-' + scene_name + '-' + scene_video_id) not in dataset_names:
            continue        
        scale = scales_yaml_content[scene_name][scene_video_id]['scale']
        sdd_dataset_i = loadSDD_single(file, scale=scale,
                                       scene_id=scene_name + scene_video_id.replace('video', ''))
        datasets.append(sdd_dataset_i)
    # ******************************

    # ========== TrajNet ==============
    # traj_dataset = TrajDataset()
    # traj_dataset.data = pd.concat(partial_datasets)
    # traj_dataset.postprocess()
    # ---------------
    # trajnet_train_root = os.path.join(opentraj_root, 'datasets/trajnet/Train')
    # trajnet_files = glob.glob(trajnet_train_root + "/**/*.txt", recursive=True)
    #
    # trajnet_datasets_list = []
    # for trajnet_file in trajnet_files:
    #     name = 'Trajnet - ' + trajnet_file.split('/')[-1][:-4]
    #     datasets_i = loadTrajNet(trajnet_file, title=name)
    #     trajnet_datasets_list.append(datasets_i)
    # trajnet_dataset = merge_datasets(trajnet_datasets_list)

    # ******************************

    return datasets
