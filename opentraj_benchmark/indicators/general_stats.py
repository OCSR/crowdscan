# Author: Javad Amirian
# Email: amiryan.j@gmail.com

import sys
import numpy as np
import pandas as pd
from crowdscan.crowd.trajdataset import TrajDataset


# def num_scenes(dataset: TrajDataset):
#     scenes = pd.unique(dataset.data["scene_id"])
#     return len(scenes)


# TODO:
def dataset_duration(dataset: TrajDataset):
    scene_ids = pd.unique(dataset.data["scene_id"])
    total_duration = 0
    for scene_id in scene_ids:
        scene_i_data = dataset.data.loc[dataset.data["scene_id"] == scene_id]
        scene_start_time = scene_i_data["timestamp"].min()
        scene_end_time = scene_i_data["timestamp"].max()
        total_duration += (scene_end_time - scene_start_time)
    return total_duration


# TODO:
def num_agents(dataset: TrajDataset):
    scene_ids = pd.unique(dataset.data["scene_id"])
    total_number = 0
    for scene_id in scene_ids:
        scene_i_data = dataset.data.loc[dataset.data["scene_id"] == scene_id]
        agent_ids_unique = pd.unique(scene_i_data["agent_id"])
        total_number += len(agent_ids_unique)
    return total_number


# TODO:
def total_trajectory_duration(dataset: TrajDataset):
    scene_ids = pd.unique(dataset.data["scene_id"])
    total_duration = 0
    for scene_id in scene_ids:
        scene_i_data = dataset.data.loc[dataset.data["scene_id"] == scene_id]
        scene_agent_ids = pd.unique(scene_i_data["agent_id"])
        for agent_id in scene_agent_ids:
            agent_i = scene_i_data.loc[scene_i_data["agent_id"] == agent_id]

            agent_start_time = agent_i["timestamp"].min()
            agent_end_time = agent_i["timestamp"].max()
            total_duration += (agent_end_time - agent_start_time)
    return total_duration


if __name__ == "__main__":
    from opentraj_benchmark.all_datasets import get_datasets, all_dataset_names
    opentraj_root = sys.argv[1]  # e.g. os.path.expanduser("~") + '/workspace2/OpenTraj'
    output_dir = sys.argv[2]  # e.g. os.path.expanduser("~") + '/Dropbox/OpenTraj-paper/exp/ver-0.2'
    datasets = get_datasets(opentraj_root, ['eth-univ'])
    # datasets = get_datasets(opentraj_root, all_dataset_names)

    for ds in datasets:
        # n_scenes = num_scenes(ds)
        dur = dataset_duration(ds)
        n_agents = num_agents(ds)
        trajs_dur = total_trajectory_duration(ds)


