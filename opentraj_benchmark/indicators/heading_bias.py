# Author: Javad Amirian
# Email: amiryan.j@gmail.com

import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import patches
# matplotlib.use('PS')
from crowdscan.crowd.trajdataset import TrajDataset


def heading_bias(dataset: TrajDataset):
    trajs = dataset.get_trajectories()
    trajlets = split_trajectories(trajs, to_numpy=True)
    n_trajlets = len(trajlets)

    start_thetas = np.arctan2(trajlets[:, 0, 2], trajlets[:, 0, 3])  # calculated from first velocity vector
    rot_matrices = np.stack([np.array([[np.cos(theta), -np.sin(theta)],
                                       [np.sin(theta), np.cos(theta)]]) for theta in start_thetas])
    trajs_zero_based = trajlets[:, :, :2] - trajlets[:, 0, :2].reshape((-1, 1, 2))

    trajs_aligned = np.matmul(rot_matrices, trajs_zero_based.transpose((0, 2, 1))).transpose((0, 2, 1))

    # filter all agents have not moved more than 2m in 5 sec
    trajlet_len = np.linalg.norm(trajs_aligned[:, -1] - trajs_aligned[:, 0], axis=1)
    non_static_trajs = trajs_aligned[trajlet_len > 2]

    keypoints = np.mean(non_static_trajs[:, :, :], axis=0)
    keypoints_radius = np.linalg.norm(keypoints, axis=1)
    keypoints_dev_avg = np.rad2deg(np.arctan2(keypoints[:, 0], keypoints[:, 1]))
    keypoints_dev_std = np.std(np.rad2deg(np.arctan2(non_static_trajs[:, :, 0],
                                                     non_static_trajs[:, :, 1])), axis=0)

    # ======== PLOT ============
    fig1, ax1 = plt.subplots()
    trajs_plt = ax1.plot(non_static_trajs[:, :, 1].T, non_static_trajs[:, :, 0].T, alpha=0.3)
    avg_plt = ax1.plot(keypoints[::2, 1], keypoints[::2, 0], 'o', color='red')

    for ii in range(2, len(keypoints), 2):
        arc_i = patches.Arc([0,0], zorder=10,
                            width=keypoints_radius[ii]*2,
                            height=keypoints_radius[ii]*2,
                            theta1=keypoints_dev_avg[ii]-keypoints_dev_std[ii],
                            theta2=keypoints_dev_avg[ii]+keypoints_dev_std[ii])
        ax1.add_patch(arc_i)

    ax1.grid()
    ax1.set_aspect('equal')
    plt.title(dataset.title)
    plt.xlim([-1.5, 10])
    plt.ylim([-4, 4])
    plt.legend(handles=[trajs_plt[0], avg_plt[0]],
               labels=["trajlets", "avg"], loc="lower left")

    # plt.savefig(os.path.join(output_dir, 'bias', name + '.png'))
    plt.show()


if __name__ == "__main__":
    from opentraj_benchmark.all_datasets import get_datasets, all_dataset_names
    from opentraj_benchmark.trajlet import split_trajectories

    opentraj_root = sys.argv[1]  # e.g. os.path.expanduser("~") + '/workspace2/OpenTraj'
    output_dir = sys.argv[2]  # e.g. os.path.expanduser("~") + '/Dropbox/OpenTraj-paper/exp/ver-0.2'
    datasets = get_datasets(opentraj_root, ['eth-univ'])
    # datasets = get_datasets(opentraj_root, ['eth-hotel'])
    # datasets = get_datasets(opentraj_root, all_dataset_names)
    for ds in datasets:
        hb = heading_bias(ds)


